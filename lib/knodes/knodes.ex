defmodule Knodes do
  @moduledoc """
  Knodes present a singlr module of a set of K (K > 1) connected nodes that form together a distributed system.

  This task is about implementing an algorithm that allows selecting a Leader from those nodes. There can only
  be one Leader at each point in time, and if Leader is unavailable, stopped, died or disappeared in a singularity
  the algorithm should elect a new Leader as soon as possible.

  Main responsibility
    - monitor leader
    - choose new leader if leader is missing

  """

  require Logger

  use GenServer

  @state_builder Application.get_env(:knodes, :state_builder)
  @timeout Application.get_env(:knodes, :timeout)
  @dead_timeout Application.get_env(:knodes, :timeout)

  def start_link(_) do
    GenServer.start_link(__MODULE__, :ok, name: __MODULE__)
  end

  @impl true
  def init(_) do
    nodes = @state_builder.build()

    Logger.info("Get states of all nodes")

    nodes =
      Knodes.Refresher.refresh(nodes, @timeout)
      |> Knodes.Election.check_if_self_is_leader()

    schedule_next_task(nodes)

    {:ok, nodes}
  end

  @doc """
  Returns current node status
  """
  @spec status() :: atom()
  def status() do
    GenServer.call(__MODULE__, :status)
  end

  @doc """
  Check if self is leader - if yes updates nodes states
  """
  @spec check_if_self_is_leader(nodes :: [Knode.Node.t()]) :: [Knode.Node.t()]
  def check_if_self_is_leader(nodes) do
    nodes =
      Knodes.NodesBag.nodes_with_bigger_ids(nodes)
      |> Knodes.Refresher.refresh(@timeout)
      |> Knodes.NodesBag.update_nodes(nodes)
      |> Knodes.Election.check_if_self_is_leader()

    leader = Knodes.NodesBag.leader(nodes)
    if leader, do: Logger.info("New leader selected #{leader.spec.id}")

    nodes
  end

  defp schedule_next_task(nodes) do
    self = Knodes.NodesBag.self(nodes)
    leader = Knodes.NodesBag.leader(nodes)

    cond do
      self.leader? ->
        Logger.info("You are the leader ")
        nil

      leader ->
        Process.send_after(self(), :ping_leader, @timeout)

      is_nil(leader) ->
        Logger.info("no leader")
        Process.send_after(self(), :elect_again, @timeout)
    end
  end

  @impl true
  def handle_info(:elect_again, nodes) do
    nodes = check_if_self_is_leader(nodes)

    schedule_next_task(nodes)

    {:noreply, nodes}
  end

  @impl true
  def handle_info(:ping_leader, nodes) do
    nodes =
      [Knodes.NodesBag.leader(nodes)]
      |> Knodes.Refresher.refresh(@dead_timeout)
      |> Knodes.NodesBag.update_nodes(nodes)

    schedule_next_task(nodes)

    {:noreply, nodes}
  end

  @impl true
  def handle_call(:status, _from, nodes) do
    self = Knodes.NodesBag.self(nodes)
    status = if self.leader?, do: :IAMTHEKING, else: :FINETHANKS
    {:reply, status, nodes}
  end
end
