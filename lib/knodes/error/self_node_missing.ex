defmodule Knodes.SelfNodeMissing do
  defexception message: "self node does not present in node list"
end
