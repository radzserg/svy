defmodule Knodes.Refresher do
  @communication Application.get_env(:knodes, :communication)

  @doc """
  Refresh nodes states, returns updated nodes

  It ping nodes with @communication and updates their states

  """
  @spec refresh(nodes :: [Knodes.NodeSpec.t()], timeout :: number()) :: Knodes.NodeSpec.t()
  def refresh(nodes, timeout) do
    node_responses =
      nodes
      |> Knodes.NodesBag.exclude_self()
      |> Enum.map(& &1.spec)
      |> @communication.ping_nodes(timeout)

    update_node_states(nodes, node_responses)
  end

  defp update_node_states(nodes, nodes_responses) do
    Enum.map(nodes, fn node ->
      response = fetch_node_response(nodes_responses, node.spec.id)

      case response do
        :FINETHANKS -> %Knodes.Node{node | alive?: true, leader?: false}
        :IAMTHEKING -> %Knodes.Node{node | alive?: true, leader?: true}
        :DEAD -> %Knodes.Node{node | alive?: false, leader?: false}
        nil -> node
      end
    end)
  end

  defp fetch_node_response(nodes, node_id) do
    node_response = Enum.find(nodes, fn {node, _response} -> node.id === node_id end)

    case node_response do
      {_node, response} -> response
      _ -> nil
    end
  end
end
