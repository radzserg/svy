defmodule Knodes.Election do
  @moduledoc """
  Responsible for new leader election
  """

  @doc """
  Select self node as leader if there are no alive nodes
  or self has max priority among other nodes
  """
  @spec check_if_self_is_leader(nodes :: [Knodes.NodeSpec.t()]) :: Knodes.NodeSpec.t()
  def check_if_self_is_leader(nodes) do
    if no_leader(nodes) && Knodes.NodesBag.self_last_alive?(nodes) do
      Knodes.NodesBag.make_self_node_leader(nodes)
    else
      nodes
    end
  end

  defp no_leader(nodes) do
    is_nil(Knodes.NodesBag.leader(nodes))
  end
end
