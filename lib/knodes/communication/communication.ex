defmodule Knodes.Communication do
  @moduledoc """
  Communication protocol specification

  Nodes can connect via HTTP, socket, erl-distribution, etc.
  This behaviour provides interface to different implementations
  """

  @doc """
  Ping multiple nodes with ALIVE? signal, gets response withing provided timeout
  Returns tuples [{Knodes.NodeSpec.t(), atom()}] where atom is response from the node
  """
  @callback ping_nodes(nodes :: [Knodes.NodeSpec.t()], timeout :: number()) :: [
              {Knodes.NodeSpec.t(), response :: atom()}
            ]
end
