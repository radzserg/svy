defmodule Knodes.ErlCommunication do
  @moduledoc """
  Node communication protocol based on ERL distribution
  """

  @behaviour Knodes.Communication

  @name Application.get_env(:knodes, :name)

  @doc """
  Ping multiple nodes with ALIVE? signal, gets response withing provided timeout
  Returns tuples [{Knodes.NodeSpec.t(), atom()}] where atom is response or nil for
  dead nodes
  """
  @impl true
  @callback ping_nodes(nodes :: [Knodes.NodeSpec.t()]) :: [{Knodes.NodeSpec.t(), atom()}]
  def ping_nodes(nodes, timeout) do
    node_addresses =
      Enum.map(nodes, fn node ->
        node.sname
      end)

    {alive, _dead} = GenServer.multi_call(node_addresses, @name, :ALIVE?, timeout)

    Enum.map(nodes, fn node ->
      address = node.sname
      state = if alive[address], do: alive[address], else: :DEAD
      {node, state}
    end)
  end
end
