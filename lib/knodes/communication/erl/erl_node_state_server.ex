defmodule Knodes.ErlNodeStateServer do
  @moduledoc """
  Process responsible to answer to other nodes
  that ping it via Knodes.ErlCommunication
  """

  use GenServer

  @name Application.get_env(:knodes, :name)

  def start_link(_) do
    GenServer.start_link(__MODULE__, :ok, name: @name)
  end

  @impl true
  def init(_) do
    {:ok, {}}
  end

  @doc """
  Get state from Knodes and respond with it
  """
  @impl true
  def handle_call(:ALIVE?, _from, nodes) do
    status = Knodes.status()
    {:reply, status, nodes}
  end
end
