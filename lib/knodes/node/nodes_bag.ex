defmodule Knodes.NodesBag do
  @moduledoc """
  Responsible for managing [Knodes.Node.t()] list. Select, update nodes
  """

  @doc """
  Finds leader node
  """
  @spec leader([Knodes.Node.t()]) :: Knodes.Node.t() | nil
  def leader(nodes), do: Enum.find(nodes, & &1.leader?)

  @doc """
  Finds self node
  """
  @spec self([Knodes.Node.t()]) :: Knodes.Node.t()
  def self(nodes), do: Enum.find(nodes, & &1.self?)

  @doc """
  Finds nodes with bigger IDs
  """
  @spec nodes_with_bigger_ids([Knodes.Node.t()]) :: [Knodes.Node.t()]
  def nodes_with_bigger_ids(nodes) do
    self = self(nodes)
    self_id = self.spec.id

    Enum.filter(nodes, fn node_state = %Knodes.Node{} ->
      node_state.spec.id > self_id
    end)
  end

  @doc """
  Updates current_nodes with provided updated_nodes,
  current node data will be overridden with updated_nodes
  """
  @spec update_nodes(
          updated_nodes :: [Knodes.Node.t()],
          current_nodes :: [Knodes.Node.t()]
        ) :: [Knodes.Node.t()]
  def update_nodes(updated_nodes, current_nodes) do
    Enum.map(current_nodes, fn current_node ->
      updated_node =
        Enum.find(updated_nodes, fn node -> node.spec.id === current_node.spec.id end)

      if updated_node, do: updated_node, else: current_node
    end)
  end

  @doc """
  Defines is self is last active node
  """
  @spec self_last_alive?(current_nodes :: [Knodes.Node.t()]) :: boolean()
  def self_last_alive?(nodes) do
    self = self(nodes)

    alive_and_bigger =
      Enum.find(nodes, fn node ->
        node.alive? && node.spec.id > self.spec.id
      end)

    is_nil(alive_and_bigger)
  end

  @doc """
  Mark self node as a leader
  """
  @spec make_self_node_leader(current_nodes :: [Knodes.Node.t()]) ::
          current_nodes :: [Knodes.Node.t()]
  def make_self_node_leader(nodes) do
    Enum.map(nodes, fn node ->
      if node.self? do
        %Knodes.Node{node | leader?: true}
      else
        %Knodes.Node{node | leader?: false}
      end
    end)
  end

  @doc """
  Exclude self node from provided list
  """
  @spec exclude_self(nodes :: [Knodes.Node.t()]) :: [Knodes.Node.t()]
  def exclude_self(nodes) do
    Enum.filter(nodes, &(!&1.self?))
  end
end
