defmodule Knodes.Node do
  @moduledoc """
  Keeps node spec and current state.
  """

  defstruct [:spec, :alive?, :self?, :leader?]

  @type t :: %Knodes.Node{
          spec: Knodes.NodeSpec.t(),
          alive?: boolean(),
          self?: boolean(),
          leader?: boolean()
        }
end
