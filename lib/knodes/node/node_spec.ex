defmodule Knodes.NodeSpec do
  @moduledoc """
  Keeps specification for a node. It could contain host address, port, sname.

  :id is required field we will use it to define priority when will elect
  leader node.

  Rest of the fields are configurable and depend on connection protocol implementation.
  In this test we are going to use ERL distribution and will keep only id and sname fields
  """

  @enforce_keys [:id]
  defstruct [:id, :sname]

  @type t :: %Knodes.NodeSpec{id: integer(), sname: atom()}
end
