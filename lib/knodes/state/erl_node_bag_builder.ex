defmodule Knodes.ErlNodesBagBuilder do
  @moduledoc """
  Builds initial state for Knodes. Deals with nodes communicating via
  Knodes.ErlCommunication

  Takes a list of nodes from config and build [Knodes.Node.t()] list.
  Makes sure that self node is presented in the list
  """

  @all_nodes Application.get_env(:knodes, :nodes)

  @doc """
  Builds initial state for the process for ERL distribution implementation.

  It builds Knodes.Spec with specifying sname, that will be used when
  for communicate between nodes
  """
  @spec build() :: [Knodes.Node.t()]
  def build() do
    self_id = define_self_id(Application.get_env(:knodes, :node_id))

    @all_nodes
    |> Enum.map(fn {id, sname} ->
      %Knodes.NodeSpec{id: id, sname: sname}
    end)
    |> Knodes.NodesBagBuilder.build(self_id)
  end

  defp define_self_id(id) when is_nil(id) do
    node_name =
      Node.self()
      |> Atom.to_string()

    matches = Regex.run(~r/node_(\d+).+/, node_name)
    id = List.last(matches)
    String.to_integer(id)
  end

  defp define_self_id(id), do: id
end
