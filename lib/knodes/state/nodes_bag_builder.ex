defmodule Knodes.NodesBagBuilder do
  @moduledoc """
  Builds initial state for Knodes
  """

  @doc """
    Build a initial state for our process.

    It will build a list of Knodes.Node from provided Knodes.NodeSpec
    Set initial state for all nodes, until we don't know there real states
    Marks self node
  """
  @spec build(nodes_specs :: [Knodes.NodeSpec.t()], self_node_address :: atom()) :: [
          Knodes.Node.t()
        ]
  def build(nodes_specs, self_id) do
    nodes_specs
    |> Enum.map(fn node = %Knodes.NodeSpec{} ->
      self? = self_id == node.id
      alive? = if self?, do: true, else: nil

      %Knodes.Node{
        spec: node,
        self?: self?,
        alive?: alive?
      }
    end)
    |> ensure_self_in_nodes
  end

  defp ensure_self_in_nodes(nodes) do
    self_presents =
      Enum.find(
        nodes,
        fn node_state = %Knodes.Node{} ->
          node_state.self?
        end
      )

    unless self_presents do
      raise Knodes.SelfNodeMissing
    end

    nodes
  end
end
