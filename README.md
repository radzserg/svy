# Knodes

**Distributed system of nodes with single leader**

Think of a set of K (K > 1) connected nodes that form together a distributed system. This task is about implementing 
an algorithm that allows selecting a Leader from those nodes. There can only be one Leader at each point in time, and 
if Leader is unavailable, stopped, died or disappeared in a singularity the algorithm should elect a new Leader as soon 
as possible.


## Requirements

elixir:1.7.* is required

## Instalation 

```bash
    git clone git@bitbucket.org:radzserg/svy.git .
    mix deps.get
```


## Development nodes

`Knodes` should be easy extensible if needed. If we want to connect nodes via REST HTTP we will need to
add 2 new modules - REST client(implementing `Knodes.Communication`) that will ask other nodes and HTTP 
the server that will respond to other nodes.

In the test ERL distribution model have been used for a demo. The test uses one local server, node connection, and 
authentication process have been omitted. It can be easily added if needed.

Application supervisor runs 2 processes `Knodes` it is responsible for monitoring other nodes and keeping current node 
status. It communicates with other nodes using `Knodes.ErlCommunication` that is the implementation of 
`Knodes.Communication` behaviour. 
The other process `Knodes.ErlNodeStateServer` is responsible to respond to other nodes. Since we used ERL distribution 
model - it communicates with other nodes sending signals via EVM. 
  
If we need socket or REST, grapqhql, etc communication protocols, new `Knodes.Communication` and `Knodes.NodeStateServer` 
implementations can be added.
   
`Knodes.Refresher` - just a higher level above, `Knodes.Communication` it checks nodes and updates `[Knodes.Node]` state

`Knodes.Election` - check if the current node can be elected as new leader.


 

## Testing 

Run N nodes, and try to kill current leader, make sure that alive node with max id become new leader.

```
elixir --sname node_1@localhost -S mix run --no-halt
elixir --sname node_2@localhost -S mix run --no-halt
elixir --sname node_3@localhost -S mix run --no-halt
...
elixir --sname node_N@localhost -S mix run --no-halt 
```

## Documentation 

Run `mix docs` to generate documentation. Documentation will be generated int `doc` folder.
