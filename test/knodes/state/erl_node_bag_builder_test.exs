defmodule KnodesTest.ErlNodesBagBuilder do
  use ExUnit.Case
  doctest Knodes.ErlNodesBagBuilder

  test "it builds default state based on config" do
    initial_state = Knodes.ErlNodesBagBuilder.build()

    expect = [
      %Knodes.Node{
        alive?: true,
        leader?: nil,
        self?: true,
        spec: %Knodes.NodeSpec{id: 1, sname: :node_1@localhost}
      },
      %Knodes.Node{
        alive?: nil,
        leader?: nil,
        self?: false,
        spec: %Knodes.NodeSpec{id: 2, sname: :node_2@localhost}
      }
    ]

    assert initial_state == expect
  end
end
