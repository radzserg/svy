defmodule KnodesTest.NodesBagBuilder do
  use ExUnit.Case
  doctest Knodes.NodesBagBuilder

  test "it builds valid state for N nodes" do
    self_id = 2
    self_node = %Knodes.NodeSpec{id: self_id, sname: :"node_2@10.10.10.2"}

    all_nodes = [
      %Knodes.NodeSpec{id: 1, sname: :"node_1@10.10.10.1"},
      self_node,
      %Knodes.NodeSpec{id: 3, sname: :"node_3@10.10.10.3"}
    ]

    nodes = Knodes.NodesBagBuilder.build(all_nodes, self_id)

    expected = [
      %Knodes.Node{
        alive?: nil,
        leader?: nil,
        spec: %Knodes.NodeSpec{id: 1, sname: :"node_1@10.10.10.1"},
        self?: false
      },
      %Knodes.Node{
        alive?: true,
        leader?: nil,
        spec: self_node,
        self?: true
      },
      %Knodes.Node{
        alive?: nil,
        leader?: nil,
        spec: %Knodes.NodeSpec{id: 3, sname: :"node_3@10.10.10.3"},
        self?: false
      }
    ]

    assert expected == nodes
  end

  test "it raises exception if self is missing" do
    self_id = 2
    all_nodes = [%Knodes.NodeSpec{id: 1, sname: :"node_1@10.10.10.1"}]

    assert_raise Knodes.SelfNodeMissing, fn ->
      Knodes.NodesBagBuilder.build(all_nodes, self_id)
    end
  end
end
