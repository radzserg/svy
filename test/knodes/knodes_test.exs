defmodule KnodesTest.Knodes do
  use ExUnit.Case
  doctest Knodes

  import Mox

  setup :verify_on_exit!

  test "it refreshes nodes and update self as leader" do
    node_1_spec = %Knodes.NodeSpec{id: 1, sname: :"node_1@10.10.10.2"}
    node_2_spec = %Knodes.NodeSpec{id: 2, sname: :"node_2@10.10.10.2"}

    nodes = [
      %Knodes.Node{
        alive?: true,
        leader?: false,
        spec: node_1_spec,
        self?: true
      },
      %Knodes.Node{
        alive?: true,
        leader?: true,
        spec: node_2_spec,
        self?: false
      }
    ]

    node_responses = [
      {node_1_spec, :FINETHANKS},
      {node_2_spec, :DEAD}
    ]

    Knodes.CommunicationMock
    |> expect(:ping_nodes, fn _, _ -> node_responses end)

    expect = [
      %Knodes.Node{
        alive?: true,
        leader?: true,
        self?: true,
        spec: node_1_spec
      },
      %Knodes.Node{
        alive?: false,
        leader?: false,
        self?: false,
        spec: node_2_spec
      }
    ]

    assert expect == Knodes.check_if_self_is_leader(nodes)
  end
end
