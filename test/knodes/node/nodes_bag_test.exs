defmodule KnodesTest.NodesBag do
  use ExUnit.Case
  doctest Knodes.NodesBag

  test "it finds leader in nodes" do
    real_leader_node = %Knodes.Node{
      alive?: true,
      leader?: true,
      spec: %Knodes.NodeSpec{id: 3, sname: :"node_2@10.10.10.2"},
      self?: true
    }

    nodes = [
      %Knodes.Node{
        alive?: true,
        leader?: false,
        spec: %Knodes.NodeSpec{id: 2, sname: :"node_3@10.10.10.3"},
        self?: false
      },
      real_leader_node
    ]

    leader = Knodes.NodesBag.leader(nodes)

    assert real_leader_node == leader
  end

  test "it selects nodes with bigger id than self id" do
    nodes = [
      %Knodes.Node{
        alive?: true,
        leader?: false,
        spec: %Knodes.NodeSpec{id: 1, sname: :"node_3@10.10.10.1"},
        self?: false
      },
      %Knodes.Node{
        alive?: false,
        leader?: false,
        spec: %Knodes.NodeSpec{id: 3, sname: :"node_3@10.10.10.3"},
        self?: false
      },
      %Knodes.Node{
        alive?: true,
        leader?: false,
        spec: %Knodes.NodeSpec{id: 2, sname: :"node_3@10.10.10.2"},
        self?: true
      },
      %Knodes.Node{
        alive?: true,
        leader?: false,
        spec: %Knodes.NodeSpec{id: 4, sname: :"node_3@10.10.10.4"},
        self?: false
      }
    ]

    found = Knodes.NodesBag.nodes_with_bigger_ids(nodes)

    expect = [
      %Knodes.Node{
        alive?: false,
        leader?: false,
        spec: %Knodes.NodeSpec{id: 3, sname: :"node_3@10.10.10.3"},
        self?: false
      },
      %Knodes.Node{
        alive?: true,
        leader?: false,
        spec: %Knodes.NodeSpec{id: 4, sname: :"node_3@10.10.10.4"},
        self?: false
      }
    ]

    assert expect == found
  end

  test "it returns true when self remain the last alive node" do
    node_1_spec = %Knodes.NodeSpec{id: 1, sname: :"node_2@10.10.10.2"}
    node_2_spec = %Knodes.NodeSpec{id: 2, sname: :"node_2@10.10.10.2"}
    node_3_spec = %Knodes.NodeSpec{id: 3, sname: :"node_3@10.10.10.3"}

    current_nodes = [
      %Knodes.Node{
        alive?: true,
        leader?: false,
        spec: node_1_spec,
        self?: false
      },
      %Knodes.Node{
        alive?: true,
        leader?: false,
        spec: node_2_spec,
        self?: false
      },
      %Knodes.Node{
        alive?: false,
        leader?: false,
        spec: node_3_spec,
        self?: true
      }
    ]

    assert Knodes.NodesBag.self_last_alive?(current_nodes)
  end

  test "it returns false when there is alive node with bigger ID" do
    node_1_spec = %Knodes.NodeSpec{id: 1, sname: :"node_2@10.10.10.2"}
    node_2_spec = %Knodes.NodeSpec{id: 2, sname: :"node_2@10.10.10.2"}
    node_3_spec = %Knodes.NodeSpec{id: 3, sname: :"node_3@10.10.10.3"}

    current_nodes = [
      %Knodes.Node{
        alive?: true,
        leader?: false,
        spec: node_1_spec,
        self?: false
      },
      %Knodes.Node{
        alive?: true,
        leader?: false,
        spec: node_2_spec,
        self?: true
      },
      %Knodes.Node{
        alive?: true,
        leader?: false,
        spec: node_3_spec,
        self?: false
      }
    ]

    refute Knodes.NodesBag.self_last_alive?(current_nodes)
  end

  test "it makes self node a leader" do
    node_1_spec = %Knodes.NodeSpec{id: 1, sname: :"node_2@10.10.10.2"}
    node_2_spec = %Knodes.NodeSpec{id: 2, sname: :"node_2@10.10.10.2"}

    current_nodes = [
      %Knodes.Node{
        alive?: true,
        leader?: true,
        spec: node_1_spec,
        self?: false
      },
      %Knodes.Node{
        alive?: true,
        leader?: false,
        spec: node_2_spec,
        self?: true
      }
    ]

    expect = [
      %Knodes.Node{
        alive?: true,
        leader?: false,
        spec: node_1_spec,
        self?: false
      },
      %Knodes.Node{
        alive?: true,
        leader?: true,
        spec: node_2_spec,
        self?: true
      }
    ]

    assert expect == Knodes.NodesBag.make_self_node_leader(current_nodes)
  end

  test "it applies updates" do
    node_1_spec = %Knodes.NodeSpec{id: 1, sname: :"node_2@10.10.10.2"}
    node_2_spec = %Knodes.NodeSpec{id: 2, sname: :"node_2@10.10.10.2"}

    current_nodes = [
      %Knodes.Node{
        alive?: true,
        leader?: true,
        spec: node_1_spec,
        self?: false
      },
      %Knodes.Node{
        alive?: true,
        leader?: false,
        spec: node_2_spec,
        self?: true
      }
    ]

    updates = [
      %Knodes.Node{
        alive?: false,
        leader?: false,
        spec: node_1_spec,
        self?: false
      }
    ]

    expect = [
      %Knodes.Node{
        alive?: false,
        leader?: false,
        spec: node_1_spec,
        self?: false
      },
      %Knodes.Node{
        alive?: true,
        leader?: false,
        spec: node_2_spec,
        self?: true
      }
    ]

    assert expect == Knodes.NodesBag.update_nodes(updates, current_nodes)
  end
end
