defmodule KnodesTest.Election do
  use ExUnit.Case
  doctest Knodes.Election

  test "self become new leader when no alive nodes" do
    node_1_spec = %Knodes.NodeSpec{id: 1, sname: :"node_2@10.10.10.2"}
    node_2_spec = %Knodes.NodeSpec{id: 2, sname: :"node_2@10.10.10.2"}
    node_3_spec = %Knodes.NodeSpec{id: 3, sname: :"node_3@10.10.10.3"}

    current_nodes = [
      %Knodes.Node{
        alive?: false,
        leader?: false,
        spec: node_1_spec,
        self?: false
      },
      %Knodes.Node{
        alive?: true,
        leader?: false,
        spec: node_2_spec,
        self?: true
      },
      %Knodes.Node{
        alive?: false,
        leader?: false,
        spec: node_3_spec,
        self?: false
      }
    ]

    expect = [
      %Knodes.Node{
        alive?: false,
        leader?: false,
        spec: node_1_spec,
        self?: false
      },
      %Knodes.Node{
        alive?: true,
        leader?: true,
        spec: node_2_spec,
        self?: true
      },
      %Knodes.Node{
        alive?: false,
        leader?: false,
        spec: node_3_spec,
        self?: false
      }
    ]

    assert expect == Knodes.Election.check_if_self_is_leader(current_nodes)
  end

  test "self become new leader when it's the last alive" do
    node_1_spec = %Knodes.NodeSpec{id: 1, sname: :"node_2@10.10.10.2"}
    node_2_spec = %Knodes.NodeSpec{id: 2, sname: :"node_2@10.10.10.2"}
    node_3_spec = %Knodes.NodeSpec{id: 3, sname: :"node_3@10.10.10.3"}

    current_nodes = [
      %Knodes.Node{
        alive?: true,
        leader?: false,
        spec: node_1_spec,
        self?: false
      },
      %Knodes.Node{
        alive?: true,
        leader?: false,
        spec: node_2_spec,
        self?: false
      },
      %Knodes.Node{
        alive?: true,
        leader?: false,
        spec: node_3_spec,
        self?: true
      }
    ]

    expect = [
      %Knodes.Node{
        alive?: true,
        leader?: false,
        spec: node_1_spec,
        self?: false
      },
      %Knodes.Node{
        alive?: true,
        leader?: false,
        spec: node_2_spec,
        self?: false
      },
      %Knodes.Node{
        alive?: true,
        leader?: true,
        spec: node_3_spec,
        self?: true
      }
    ]

    assert expect == Knodes.Election.check_if_self_is_leader(current_nodes)
  end
end
