defmodule KnodesTest.Refresher do
  use ExUnit.Case, async: true
  doctest Knodes.Refresher

  import Mox

  setup :verify_on_exit!

  test "it refreshes nodes statuses" do
    nodes = [
      %Knodes.Node{
        alive?: false,
        leader?: false,
        spec: %Knodes.NodeSpec{id: 1, sname: :"node_1@10.10.10.1"},
        self?: false
      },
      %Knodes.Node{
        alive?: true,
        leader?: true,
        spec: %Knodes.NodeSpec{id: 2, sname: :"node_2@10.10.10.2"},
        self?: true
      },
      %Knodes.Node{
        alive?: false,
        leader?: false,
        spec: %Knodes.NodeSpec{id: 3, sname: :"node_3@10.10.10.3"},
        self?: false
      },
      %Knodes.Node{
        alive?: true,
        leader?: false,
        spec: %Knodes.NodeSpec{id: 4, sname: :"node_4@10.10.10.4"},
        self?: false
      }
    ]

    node_responses = [
      {%Knodes.NodeSpec{id: 1, sname: :"node_1@10.10.10.1"}, :FINETHANKS},
      {%Knodes.NodeSpec{id: 2, sname: :"node_2@10.10.10.2"}, :DEAD},
      {%Knodes.NodeSpec{id: 3, sname: :"node_3@10.10.10.3"}, :IAMTHEKING}
    ]

    timeout = 1000

    Knodes.CommunicationMock
    |> expect(:ping_nodes, fn _, ^timeout -> node_responses end)

    expect = [
      %Knodes.Node{
        alive?: true,
        leader?: false,
        spec: %Knodes.NodeSpec{id: 1, sname: :"node_1@10.10.10.1"},
        self?: false
      },
      %Knodes.Node{
        alive?: false,
        leader?: false,
        spec: %Knodes.NodeSpec{id: 2, sname: :"node_2@10.10.10.2"},
        self?: true
      },
      %Knodes.Node{
        alive?: true,
        leader?: true,
        spec: %Knodes.NodeSpec{id: 3, sname: :"node_3@10.10.10.3"},
        self?: false
      },
      %Knodes.Node{
        alive?: true,
        leader?: false,
        spec: %Knodes.NodeSpec{id: 4, sname: :"node_4@10.10.10.4"},
        self?: false
      }
    ]

    assert expect == Knodes.Refresher.refresh(nodes, timeout)
  end
end
