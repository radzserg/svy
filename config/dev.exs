use Mix.Config

config :knodes,
  nodes: [
    {1, :node_1@localhost},
    {2, :node_2@localhost},
    {3, :node_3@localhost},
    {4, :node_4@localhost}
  ]
