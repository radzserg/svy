use Mix.Config

config :logger, level: :warn

config :knodes,
  node_id: 1,
  nodes: [
    {1, :node_1@localhost},
    {2, :node_2@localhost}
  ],
  communication: Knodes.CommunicationMock
