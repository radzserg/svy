# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
use Mix.Config

config :knodes,
  name: :knodes,
  state_builder: Knodes.ErlNodesBagBuilder,
  communication: Knodes.ErlCommunication,
  timeout: 1000,
  dead_timeout: 4000

import_config "#{Mix.env()}.exs"
